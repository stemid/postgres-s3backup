#!/usr/bin/env bash

cmd=${1:-list} && shift
env=${BACKUP_ENV:-default}
compression=${BACKUP_COMPRESSION:-zstd}

set -a && \
  test -r "./env.$env.sh" && source "./env.$env.sh" && \
  set +a

if [ "$cmd" = 'backup' ]; then
  currentTimestamp=$(date +'%Y%m%d.%H%M%S.%s')
  pg_dump | "$compression" | \
    gpg2 --output - --encrypt -r "$GPG_RECIPIENT" "${GPG_EXTRA_ARGS[@]}" - | \
    aws s3 cp - "s3://$AWS_BUCKET/postgres/$PGDATABASE-$currentTimestamp.sql.${compression}.gpg"
fi

if [ "$cmd" = 'list' ]; then
  aws s3 ls "$@" "s3://$AWS_BUCKET/postgres/"
fi

if [ "$cmd" = 'prune' ]; then
  max=${1:-5} && shift
  c=0
  readarray -t backups < <(aws s3 ls "s3://$AWS_BUCKET/postgres/" | sort -rn)
  for backup in "${backups[@]}"; do
    ((c++))
    IFS=' ' read -ra fields <<<"$backup"
    filename=${fields[3]}
    if [ "$c" -gt "$max" ]; then
      aws s3 rm "s3://$AWS_BUCKET/postgres/$filename"
    fi
  done
fi

if [ "$cmd" = 'restore' ]; then
  readarray -t backups < <(aws s3 ls "s3://$AWS_BUCKET/postgres/" | sort -rn)
  for backup in "${backups[@]}"; do
    IFS=' ' read -ra fields <<<"$backup"
    filename=${fields[3]}
    aws s3 cp "s3://$AWS_BUCKET/postgres/$filename" - | \
      gpg2 --decrypt - | "$compression" -d 
    break
  done
fi
