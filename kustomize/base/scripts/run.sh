#!/usr/bin/env bash

test -r "$BACKUP_KEY" && echo "$BACKUP_KEY" | gpg2 --import -
test -r "$RCPT_KEY" && echo "$RCPT_KEY" | gpg2 --import -

/bin/bash s3backup.sh backup && \
  /bin/bash s3backup.sh prune 14
