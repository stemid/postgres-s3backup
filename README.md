# PostgreSQL Backup to AWS S3 with GnuPG encryption

I needed this to run as a backup job.

Note that this requires;

1. Working GnuPG2 setup with at least two public keys imported and a matching identity as recipient.
2. AWS IAM user with access to the bucket you want to use.
3. AWS S3 Bucket setup (without public access).
4. awscli

## GPG

### Generate a key for your backup user

    $ gpg2 --gen-key
    Name: S3Backup

### Export your backup users public key

    $ gpg2 --export --armor 'S3Backup'

### Export your own public key

    $ gpg2 --export --armor 'Your name'

### Import the keys on the backup control system

Import your backup user first and it will automatically be used to encrypt.

    $ gpg2 --import -
    <paste your backup users public key>
    Ctrl+d for EOF
    $ gpg2 --import -
    <paste your own public key>
    Ctrl+d for EOF

## Configure environment settings

* Copy ``env.default.sh`` to your own file and make your own changes.
* Set ``GPG_RECIPIENT='Your name'`` to the identity of your own gpg key.
* Ensure the PG settings work by sourcing the file and running ``pg_dump`` without any arguments.

    $ cp env.default.sh env.dev.sh

## Backup

    $ BACKUP_ENV=dev bash s3backup.sh backup

## List backups

Can take additional arguments for [awscli s3 ls](https://docs.aws.amazon.com/cli/latest/reference/s3/ls.html).

    $ bash s3backup.sh list --human-readable --sumarize
    2020-10-27 21:45:04 1014 Bytes postgres-20201027.214502.1603831502.sql.xz.gpg
    Total Objects: 1
       Total Size: 1014 Bytes

## Prune oldest backup

    $ bash s3backup.sh prune

Or only save the last 5 backups.

    $ bash s3backup.sh prune 5

## Restore last backup

    $ bash s3backup.sh restore > restored.sql
