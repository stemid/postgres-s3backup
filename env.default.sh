export AWS_ACCESS_KEY_ID=secret
export AWS_SECRET_ACCESS_KEY=secret
export AWS_DEFAULT_REGION=eu-north-1
export AWS_BUCKET=bucket

export GPG_RECIPIENT='My own identity'
export GPG_EXTRA_ARGS=(
# Use this if you only have public keys to encrypt with.
#  --trust-model always
)

export PGHOST=localhost
export PGDATABASE=postgres
export PGUSER=postgres
export PGSSLMODE=disable
export PGPASSWORD=postgres
